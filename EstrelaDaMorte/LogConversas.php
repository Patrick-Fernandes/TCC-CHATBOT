<?php
include './includes/cabeca.php';
include './Buscas/Lista_pergunta.php';

if (!isset($_GET['pagina']))
{
    $pc = "1";
}
else
{
    $pc = $_GET['pagina'];
}
$total_reg = "50";
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
$limite = logConversa2($inicio, $total_reg);
$todos = logConversa();
$result = $limite;
$tr = mysqli_num_rows($todos); // verifica o número total de registros
$tp = $tr / $total_reg;
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Área Administrativa</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <!-- /.panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i>Lista de Logs        
                </div>

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table-responsive table">
                                    <thead class="thead-inverse">
                                        <tr>

                                            <TH>ID</TH>
                                            <TH>Conversa</TH>
                                            <th>IP</th>
                                            <th>Data de envio</th>
                                            <th>Data de fim</th>
                                            <th>Todo  L</th>
                                        <tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $result = logConversa();
                                        if (mysqli_num_rows($result) > 0)
                                        {
                                            // output data of each row
                                            while ($row = mysqli_fetch_assoc($result))
                                            {
                                                $string = mb_strimwidth($row["conversa"], 0, 10, "...");
                                                ?>

                                                <tr class="form-group">
                                                    <td><?= $row["id_log_conversa"] ?></td>       
                                                    <td><?= $string ?></td>
                                                    <td><?= $row["ip"] ?></td>
                                                    <td><?= $row["data_inc"] ?></td>
                                                    <td><?= $row["data_fim"] ?></td>
                                                    <td> <a href="ListaLog<?php echo $row["id_log_conversa"] ?>"><br>Ver resto do log</a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        else
                                        {
                                            ?>
                                            <tr>
                                                <td>Sem nenhum resultado</td>
                                            </tr>    
                                            <?php
                                        }
                                        ?>

                                    <tbody>


                                </table>

                            </div>
                            <!-- /.table-responsive -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-4 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
<?php
include './includes/rodape.php';
